import {Component, OnInit} from '@angular/core';
import {HomeService} from './home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  topMovies: any = [];
  comingSoonMovies: any = [];

  constructor(private homeService: HomeService) {
  }

  ngOnInit() {
    this.getTotMovies();
  }

  getTotMovies() {
    this.homeService.getTopMovies().subscribe(
      response => {

        console.log('response', response);

        this.topMovies = response['data'] && response['data'].movies;
        this.getComingSoonMovies();
      },
      err => console.error(err),
      () => {
      }
    );
  }

  getComingSoonMovies() {
    this.homeService.getComingSoonMovies().subscribe(
      response => {

        console.log('response', response);

        this.comingSoonMovies = response['data'] && response['data'].movies;
      },
      err => console.error(err),
      () => console.log('done loading movies')
    );
  }
}
