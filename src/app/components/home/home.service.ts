import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  topMoviesUrl = '/imdb/top?start=1&end=12&token=4b648161-b566-4d4f-a73c-8c1ade6680f6&format=json&data=0';
  commingSoonMovies = '/imdb/comingSoon?start=1&end=12&token=4b648161-b566-4d4f-a73c-8c1ade6680f6&format=xml&language=en-us&date=2018-09';

  constructor(private http: HttpClient) {
  }

  getTopMovies() {
    return this.http.get(this.topMoviesUrl);
  }

  getComingSoonMovies() {
    return this.http.get(this.commingSoonMovies);
  }
}
