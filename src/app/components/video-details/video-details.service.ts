import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VideoDetailsService {
  videoUrl = '/imdb/idIMDB?idIMDB=';

  constructor(private http: HttpClient) {
  }

  getVideoDetails(id) {
    return this.http.get(`${this.videoUrl + id}&token=4b648161-b566-4d4f-a73c-8c1ade6680f6`);
  }
}
